from setuptools import setup
from os import path

try:
    README = open(path.join(
        path.dirname(__file__), "README.md")).read()
except IOError:
    README = ''

setup(name='s3uploader',
        version='0.0.1',
        description='Upload directly to S3 via dropzone.js.',
        long_description=README,
        url='http://bitbucket.com/michaelb/django-s3-uploader/',
        author='michaelb',
        author_email='michaelpb@gmail.com',
        license='GPL 3.0',
        include_package_true=True,
        packages=['s3uploader'],
        package_dir={ 's3uploader': 's3uploader' },
        #package_data={ 's3uploader': ['static/*'] },
        zip_safe=False)

