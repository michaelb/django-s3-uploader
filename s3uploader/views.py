# Copyright 2014 michaelb
# Released under the GPL 3.0

# Some code here is inspired by the excellent server-side examples provided by
# Widen under the MIT license https://github.com/Widen/fine-uploader-server/

# python
import json

# django
from django.views.generic.base import View
from django.http import HttpResponse

from . import util
from django.conf import settings

global_opts = settings.S3UPLOADER

class JsonResponse(HttpResponse):
    "Simple class to give JSON response"
    def __init__(self, data={}, status=200):
        s = json.dumps(data)
        super(JsonResponse, self).__init__(s, mimetype='application/json')
        self.status_code = status

def blankpage(request):
    """
    Blank page required for old browser support hack
    """
    return HttpResponse("", content_type="text/html")


def _variables(request):
    """
    Extracts page variables from request (e.g. context variables, etc, all
    subject to user tampering of course)
    """
    variables = request.META.get(
            util.VARIABLES_HTTP_HEADER_DJANGO) or None

    if not variables:
        variables = request.POST.get('variables')

    if variables:
        # Assume is a JSON-encoded string
        return json.loads(variables)
    else:
        # Or default to empty dict if not found
        return {}


def _config_name(request):
    """
    Extract configuration profile choice from page request
    """
    config_name = request.META.get(
            util.CONFIG_NAME_HTTP_HEADER_DJANGO) or None
    if not config_name:
        config_name = request.POST.get('config_name')
    return config_name



class BaseView(View):
    def _backend(self, request):

        # Get the requested config name value
        config_name = _config_name(request)

        self.opts = util.get_settings(config_name)

        backend = self.opts['backend']
        variables = _variables(request)

        if not backend.allow(request, variables):
            return JsonResponse({'invalid': True,
                    "reason": "disallowed (key)"}, status=403)

        return backend


class CreateKey(BaseView):
    """
    Creates filename (i.e. S3 key) for given file
    """
    def post(self, request):
        backend = self._backend(request)
        if isinstance(backend, JsonResponse):
            return backend

        # Get variable values out of request
        variables = _variables(request)

        filename = request.POST.get('filename')
        target_path = backend.starting_upload(filename, request, variables)

        return JsonResponse({
                'key': target_path,
            })


class Success(BaseView):
    """
    Triggers "success" event
    """
    def post(self, request):
        backend = self._backend(request)
        d = {}

        # Get variable values out of request
        variables = _variables(request)

        # Shortcut
        if isinstance(backend, JsonResponse):
            return backend

        p = lambda k: request.POST.get(k, [])
        #successes = zip(p('name'), p('key'), p('bucket'), p('uuid'))
        #for name, key, bucket, uuid in successes:

        filenames = request.POST.get('name')
        if not isinstance(filenames, list):
            filenames = [filenames]

        for name in filenames:
            d.update(backend.success(name, request, variables))

        return JsonResponse(d)


class Signature(View):
    """
    Base view class to perform signature operation
    """
    def is_valid_policy(self, policy_document, opts):
        """
        Verify the policy document has not been tampered with client-side
        """
        bucket = ''
        parsed_max_size = 0

        BUCKET_NAME = opts["bucket_name"]

        # Loop through policy document
        for condition in policy_document['conditions']:
            if isinstance(condition, list) and condition[0] == 'content-length-range':
                parsed_max_size = condition[2]
            elif condition.get('bucket'):
                bucket = condition['bucket']

        if opts.get('max_size') and parsed_max_size != opts.get('max_size'):
            # If a max size is specified and the content-length-range is not
            # equal to it, then we reject
            return False

        # Basic check, make sure it goes in the right bucket
        return bucket == BUCKET_NAME


    def before_response(self, data):
        return data


    def post(self, request):
        """
        Signs off request to upload to S3

        For files <=5MiB this is a simple request to sign the policy document.
        For files >5MiB this is a request to sign the headers to start a
        multipart encoded request.
        """

        # todo: Not sure what this is for?
        if request.POST.get('success'):
            return JsonResponse()

        # Get variable values out of request
        variables = _variables(request)
        config_name = _config_name(request)

        opts = util.get_settings(config_name)

        PUBLIC_KEY = opts["access_key_id"]
        SECRET_KEY = opts["secret_access_key"]

        request_payload = json.loads(request.body)

        backend = opts['backend']

        if not backend.allow(request, variables):
            return JsonResponse({'invalid': True,
                    "reason": "disallowed (sig)"}, status=403)

        headers = request_payload.get('headers')

        if headers:
            # The presence of the 'headers' property in the request payload
            # means this is a request to sign a REST/multipart request and NOT
            # a policy document
            response_data = util.sign_chunked_headers(headers, SECRET_KEY)
        else:
            if not self.is_valid_policy(request_payload, opts):
                return JsonResponse({'invalid': True,
                    "reason": "invalid policy"}, status=403)
            response_data = util.sign_policy_document(request_payload, SECRET_KEY)

        response_data = self.before_response(response_data)

        return JsonResponse(response_data)



