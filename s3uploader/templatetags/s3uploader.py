# python
import json
import random

# django
from django.conf import settings
from django.utils.translation import ugettext as _
from django.utils.html import conditional_escape, escape
from django.utils.safestring import mark_safe
from django.template.loader import render_to_string
from django.middleware.csrf import get_token
from django import template
from django.core.exceptions import ImproperlyConfigured


# import util class
from .. import util


register = template.Library()

class S3UploaderFine(template.Node):
    TEMPLATE = "s3uploader/snippets/fineuploader.html"
    def __init__(self, config_name=None, variables={}):
        opts = util.get_settings(config_name)

        ctx = {
                'STATIC_URL': settings.STATIC_URL,
                'opts': opts,
                'config_name': config_name,
                'config_name_http_header': util.CONFIG_NAME_HTTP_HEADER,
                'access_key_id': opts['access_key_id'],
                'rand_id': str(random.random()).strip('0.'),
                'template_name': opts['template_name'],
            }

        # JSON-ified properties
        for s in ('validation', 'params', 'paste', 'options'):
            if opts.get(s):
                ctx['%s_json'%s] = json.dumps(opts[s])

        self.rendered = render_to_string(self.TEMPLATE, ctx)

        self.variables = {}
        for var_name, var_val in variables.items():
            self.variables[var_name] = template.Variable(var_val)


    def render(self, context):
        request = context.get('request')

        if not request:
            raise ImproperlyConfigured("s3uploader: 'request' not found in "
                    "context, should add using context_processor, or "
                    "properly using render() shortcut")

        csrf_token = get_token(request)

        # simple replace to insert token
        s = self.rendered.replace("$csrf_token$", csrf_token)
        variables = {}
        for var_name, var in self.variables.items():
            variables[var_name] = var.resolve(context)

        variables_str = escape(json.dumps(variables))

        # variables to pass on to all AJAX requests
        s = s.replace("$variables$", variables_str)

        return s


@register.tag(name="s3uploader")
def s3uploader(parser, token):
    """
    This creates a FineUploader for the uploader
    """
    contents = token.split_contents()
    variables = {}

    # pop off "s3uploader"
    contents.pop(0)

    try:
        config_name = contents.pop(0)
    except IndexError:
        config_name = None
    else:
        if '=' in config_name:
            # just contents, not a configuration
            contents.push(config_name)
            config_name = None

        for assignment in contents:
            varname, _, varval = assignment.partition('=')
            variables[varname] = varval

    return S3UploaderFine(config_name, variables)


def _script_tags(*paths):
    return ''.join([('<script src="%ss3uploader/js/%s"></script>' %
                    (settings.STATIC_URL, path)) for path in paths])


@register.tag(name="s3uploader_js")
def s3uploader_js(parser, token):
    """
    This outputs necessary script tags for the uploader
    """
    s = _script_tags('init_fineuploader.js', 's3.jquery.fineuploader-4.1.1.js')
    return template.base.TextNode(s)


@register.tag(name="s3uploader_css")
def s3uploader_css(parser, token):
    """
    This outputs necessary css tags for the uploader
    """
    s = """
        <link href="%ss3uploader/css/fineuploader-4.1.1.css" rel="stylesheet">
        """.replace('  ', '') % settings.STATIC_URL
    return template.base.TextNode(s)



