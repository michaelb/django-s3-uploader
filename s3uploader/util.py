# Copyright 2014 michaelb
# Released under the GPL 3.0

# Some code here is inspired by the excellent server-side examples provided by
# Widen under the MIT license https://github.com/Widen/fine-uploader-server/

import base64
import hmac
import hashlib
import json
from importlib import import_module

# django
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.apps import apps


def sign_chunked_headers(headers, secret_key):
    """
    Sign and return the headers for a chunked upload.
    """
    #digest = hmac.new(settings.AWS_CLIENT_SECRET_KEY, headers, hashlib.sha1).digest()
    digest = hmac.new(secret_key, headers, hashlib.sha1).digest()
    return {
        'signature': base64.b64encode(digest)
    }


def sign_policy_document(policy_document, secret_key):
    """
    Sign and return the policy document for a simple upload.
    http://aws.amazon.com/articles/1434/#signyours3postform
    """
    policy = base64.b64encode(json.dumps(policy_document))
    #signature = base64.b64encode(hmac.new(settings.AWS_CLIENT_SECRET_KEY, policy, hashlib.sha1).digest())
    digest = hmac.new(secret_key, policy, hashlib.sha1).digest()
    signature = base64.b64encode(digest)
    return {
        'policy': policy,
        'signature': signature
    }



__conf_cache = {}

def get_settings(conf_name=None):
    """
    Returns a dict of settings based on inheritence model (with 'default'
    being both default one and also the one that the ohers inherits from, and
    if there is no default then it just assumes that the configuration is
    loose inside the dict and there are not separate conf groups.
    """

    # Cache configuration so this conf / default logic only runs once per conf
    if __conf_cache.get(conf_name):
        return __conf_cache[conf_name]

    # Global opts
    gopts = getattr(settings, 'S3UPLOADER', {})
    mopts = getattr(settings, 'S3UPLOADER_PROFILES', {})


    # Update as needed with specific configuration set
    if conf_name:

        # Get base dict opts, using dict() to make a clone
        opts = dict(mopts.get('default', gopts))

        if conf_name not in mopts:
            e = ("s3uploader: Could not find configuration profile "
                "'%s' in S3UPLOADER_PROFILES" % conf_name)
            raise ImproperlyConfigured(e)

        opts.update(mopts[conf_name])
    else:
        opts = dict(gopts)

    # Examples
    # AWS_ACCESS_KEY_ID = 'XXXXXXXXXXXXXXXXXXXX'
    # AWS_SECRET_ACCESS_KEY = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
    # AWS_S3_SECURE_URLS = False
    # AWS_STORAGE_BUCKET_NAME = 'XXXXXXXXXXXX'

    # Check for required opts
    def check_and_set(opts_name, g_name, default=None):
        if not opts_name in opts:
            if default != None:
                opts[opts_name] = getattr(settings, g_name, default)
                return

            if not hasattr(settings, g_name):
                raise ImproperlyConfigured("s3uploader: Must specify either "+
                        "'%s' in options dict, or %s " % (opts_name, g_name)+
                        "in global settings")

            opts[opts_name] = getattr(settings, g_name)

    check_and_set('access_key_id', 'AWS_ACCESS_KEY_ID')
    check_and_set('secret_access_key', 'AWS_SECRET_ACCESS_KEY')
    check_and_set('bucket_name', 'AWS_STORAGE_BUCKET_NAME')
    check_and_set('generic_model_class', 'S3UPLOADER_GENERIC_MODEL_CLASS', '')

    # Non required (False is default)
    check_and_set('secure_urls', 'AWS_S3_SECURE_URLS', False)

    opts.setdefault("bucket_url", "http%s://%s.s3.amazonaws.com/" %
                (('s' if opts['secure_urls'] else ''), opts['bucket_name']))

    opts.setdefault("template_name", "default")
    opts.setdefault("options", {})

    # General defaults
    options = {
            'retry': { 'enableAuto': True },
            'chunking': { 'enabled': True },
            'resume': { 'enabled': True },
        }

    # Options
    options.update(opts.get('options', {}))
    opts['options'] = options

    if opts.get('disable_css_classes'):
        options.setdefault('classes', {})
        new_classes = {
                'buttonFocus': '',
                'buttonHover': '',
                'dropActive': '',
                'editable': '',
                'fail': '',
                'retryable': '',
                'retrying': '',
                'success': '',
            }
        new_classes.update(options['classes'])
        options['classes'] = new_classes

    backend_class = None
    backend_class_name = ''
    if opts.get('generic_model_class'):
        backend_class_name = 's3uploader.backends.GenericUploadableBackend'
        app_name, model_name = opts['generic_model_class'].split('.', 1)
        opts['generic_model_class'] = apps.get_model(app_name, model_name)
        if not opts['generic_model_class']:
            raise ImproperlyConfigured("s3uploader: Could not find "
                    "generic_model_class '%s.%s'." % (app_name, model_name))


    if opts.get('backend'):
        # Instantiate backend
        app_name, model_name = opts['generic_model_class'].rsplit('.', 1)

    if backend_class_name:
        app_name, model_name = backend_class_name.rsplit('.', 1)
        # actually try to import module
        module = import_module(app_name)
        opts['backend'] = getattr(module, model_name)(opts)
    else:
        raise ImproperlyConfigured("s3uploader: Must specify either "
                "'backend' or 'generic_model_class' in options dict. Example:\n"
                "'backend': yourapp.s3uploader_backends.PhotoBackend\n"
                "Where 'PhotoBackend' is a object that extends the "
                "s3uploader.backends.Backend base class")


    opts['_conf_name'] = conf_name
    __conf_cache[conf_name] = opts

    return opts


HTTP_HEADER_PREFIX = 'X-DjangoS3Uploader-'

def http_header(suffix, for_django=False):
    s = HTTP_HEADER_PREFIX + suffix
    if for_django:
        s = "HTTP_" + s
        return s.replace('-', '_').upper()
    
    return s

CONFIG_NAME_HTTP_HEADER        = http_header('ConfName')
CONFIG_NAME_HTTP_HEADER_DJANGO = http_header('ConfName', True)

VARIABLES_HTTP_HEADER          = http_header('Variables')
VARIABLES_HTTP_HEADER_DJANGO   = http_header('Variables', True)

