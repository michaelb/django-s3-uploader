
SESSION_KEY = 's3gum_model_pks'

def _session_key(options, original_filename):
    conf_name = options['_conf_name'] or 'NONE'
    #return ":".join([SESSION_KEY, conf_name, original_filename])
    return ":".join([SESSION_KEY, conf_name, str(abs(hash(original_filename)))])

class BackendBase(object):
    """
    Abstract class
    """
    def __init__(self, options):
        self.options = options

    ######################################
    # Override these...
    def starting_upload(self, original_filename, request, variables):
        raise Exception("starting_upload not implemented in Backend class")

    def allow(self, request, variables):
        return True

    def success(self, request, variables):
        return {}
    ######################################


class GenericUploadableBackend(BackendBase):
    """
    Default backend if only a Generic Uploadable model class is specified.
    This backend will automatically instantiate
    """
    def __init__(self, options):
        self.options = options

    ######################################
    def starting_upload(self, original_filename, request, variables):
        cls = self.options['generic_model_class']

        def _make_url(filename):
            url_prefix = self.options.get('url_field_bucket_url', 
                            self.options.get('bucket_url'))
            return url_prefix + filename

        obj = cls.s3_get_object(original_filename, request, variables)
        desired_s3_key = obj.set_starting_s3_upload(
            original_filename, request, variables, _make_url)
        obj.save()

        key = _session_key(self.options, original_filename)
        request.session[key] = obj.id

        return desired_s3_key

    def allow(self, request, variables):
        cls = self.options['generic_model_class']
        return cls.s3_should_allow_request(request)

    def success(self, original_filename, request, variables):

        key = _session_key(self.options, original_filename)
        obj_id = request.session.get(key)

        if not obj_id:
            raise Exception("Could not retrieve object ID from session data "
                        "(Key: %s) (filename: %s)" % (key, original_filename))

        cls = self.options['generic_model_class']
        obj = cls.objects.get(id=obj_id)

        #try:
        #    obj = cls.objects.get(id=obj_id)
        #except cls.DoesNotExist:
        #    raise Exception("Couldn't find object with that '%s' - '%s'" %
        #                                (obj_id, original_filename))

        obj.set_ending_s3_upload(request, variables)
        obj.save()

        return {}

    ######################################


