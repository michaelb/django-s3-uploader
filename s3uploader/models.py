import hashlib

from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from django.template.defaultfilters import slugify


class GUMError(AttributeError): pass
class GUMPropertyError(GUMError): pass

class GenericUploadableMixin(object):

    @classmethod
    def __gum_conf(cls, name, default=None):
        # "Generic Uploader Model Configuration" 
        if not hasattr(cls, 'S3UploadableMeta'):
            raise GUMError("Models which extend GenericUploadableMixin must"
                " specify S3UploadableMeta data, see docs.")

        meta = cls.S3UploadableMeta

        if hasattr(meta, name):
            return getattr(meta, name)

        elif default is None:
            raise GUMPropertyError("S3UploadableMeta expected to have "
                    "'%s' property specified" % name)
        else:
            return default


    def __gum_set(self, name, value, allow_unspecified=None):
        # "Generic Uploader Model Set"  --- sets field
        fieldname = self.__gum_conf(name, allow_unspecified)
        if fieldname is allow_unspecified:
            return

        setattr(self, fieldname, value)


    def __gum_get(self, name, default=None):
        # "Generic Uploader Model Set"  --- gets field
        fieldname = self.__gum_conf(name, default)
        if fieldname is default:
            return default

        return getattr(self, fieldname)


    @classmethod
    def __gum_trigger(cls, name, args=[], kwds={}, default_value=None):
        func = getattr(cls.S3UploadableMeta, name, None)
        if func:
            return func(*args, **kwds)
        return default_value


    @classmethod
    def s3_should_allow_request(cls, request):

        if not cls.__gum_trigger('allow', request, True):
            return False

        if cls.__gum_conf('user_field', False):
            return hasattr(request, 'user') and request.user.is_authenticated()

        return True

    @classmethod
    def s3_get_object(cls, original_filename, request, variables):
        """
        Either return an existing object or something
        """
        meta = cls.S3UploadableMeta
        if hasattr(meta, 'get_object'):
            return meta.get_object(request, original_filename, variables)
        return cls()


    def set_starting_s3_upload(self, original_filename, request, variables, make_url):
        """
        Given a origininal_filename, request, and page variables, it sets the
        upload as "starting"
        """
        if self.__gum_conf('file_field', False):
            field_file = self.__gum_get('file_field')
            file_field = field_file.field

            # Using undocumented "generate_filename" method on the file field
            # to generate desired destination
            desired_s3_key = file_field.generate_filename(self, original_filename)

            # Finally assigns
            field_file.name = desired_s3_key

        elif self.__gum_conf('url_field', False):
            # URL field
            desired_s3_key = self.__gum_trigger(
                            'generate_filename', (self, original_filename),
                            # By default just lets user choose completely
                            default_value=original_filename)
            full_url = make_url(desired_s3_key)
            self.__gum_set('url_field', full_url)

        # Sets, if available, the ready file field bit to "off"
        self.__gum_set('is_ready_field', False, allow_unspecified=True)

        # trigger any extra stuff on upload start
        self.__gum_trigger('on_upload_start', (self, request,))

        return desired_s3_key


    def set_ending_s3_upload(self, request, variables):
        # Sets is_ready_field to be True
        self.__gum_set('is_ready_field', True, allow_unspecified=True)

        # trigger any custom events
        self.__gum_trigger('on_upload_end', (self, request,))


