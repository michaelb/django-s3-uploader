# -*- coding: utf-8 -*-
from django.conf.urls import include, url
from django.conf import settings

from .views import Signature, CreateKey, Success, blankpage

try:
    # try for custom signature view class
    SignatureViewClass = settings.S3DIRECT_SIGNATURE_VIEW
except AttributeError:
    # otherwise go with base
    SignatureViewClass = Signature

urlpatterns = [
    # blank page required for old browser support hack
    url(r'^ajax/blankpage/$', blankpage, {}, 's3uploader_blankpage'),
    url(r'^ajax/signature/$', SignatureViewClass.as_view(), {}, 's3uploader_signature_endpoint'),
    url(r'^ajax/upload-success/$', Success.as_view(), {}, 's3uploader_upload_success_endpoint'),
    url(r'^ajax/get-key/$', CreateKey.as_view(), {}, 's3uploader_create_key'),
]
