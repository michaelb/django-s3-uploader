
*NOTE:* this is not yet fully documented, and does not have
a proper test suite.



Drop in Django FineUploader integration for direct Amazon S3 uploads.

Direct S3 uploads are uploads *directly* to your Amazon S3 account from the
user's web browser, never touching your servers. This allows for extremely
scalable web apps, since you don't need to worry about upload servers.

Because of Fine Uploader support, it has advanced  features like chunking, no
real file size limit, upload resuming, drag and drop, and quality cross
platform (including old-ish IE versions).

FineUploader
================

FineUploader is an amazing piece of software.
[Check out their site](http://fineuploader.com/) for information on support for
this open source piece of software. This software package (django-s3-uploader),
is maintained separately and is totally unrelated to FineUploader.

Overview
================

It lets you add easily include the extremely powerful FineUploader to upload
*mulitple* files to S3. It uses a extensible backend system  to let you add in
your own code for creating models, determining file names, and deciding
permissions. For example, you might include a widget that only accepts image
files. When a user drops in a bunch of image files, it makes AJAX requests to
create models for each with file storage properly set, saving each, and then
letting the upload commence after the DB stuff has been set up.

It is an free software project developed for [OpenLab](http://openlab.org/), the
free/libre hardware collaboration and publishing community.


Configuration
==============

Set up
---------------

1. Add `s3uploader` to installed apps. Example:

    INSTALLED_APPS = INSTALLED_APPS + ('s3uploader',)

2. Add `s3uploader` URLs to your app's URL conf:

3. Configure your S3 bucket to allow for direct upload. FineUploader has a nice
   [step-by-step guide on how to do this](http://blog.fineuploader.com/2013/08/16/fine-uploader-s3-upload-directly-to-amazon-s3-from-your-browser/).

4. Use one of the following configuration methods:


Configuration method 1
---------------

If you already have AWS configured as follows, then you're good to go:

      AWS_ACCESS_KEY_ID = 'XXXXXXXXXXXXXXXXXXXX'
      AWS_SECRET_ACCESS_KEY = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
      AWS_S3_SECURE_URLS = False
      AWS_STORAGE_BUCKET_NAME = 'XXXXXXXXXXXX'

Configuration method 2
---------------

The preferred method is with a dictionary:

    S3UPLOADER = {
        'bucket_name': 'openlab-testing',
        'template_name': 'bootstrap',   # options are 'default', 'simple-thumbnail', and 'bootstrap'

        'access_key_id': 'XXXXXXXXXXXXXXXXXXXX',
        'secret_access_key': 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
        's3_secure_urls': False,
    }


Configuration method 3: multiple configuration profiles
---------------


If you will be having multiple configuration profiles in a single django
project, for example one for uploading small images for profile pictures with
one particular theme, the other for uploading massive videos, then you will
want to configure as follows:

    S3UPLOADER_PROFILES = {
        'default': {
            'bucket_name': 'openlab-testing',
            'access_key_id': 'XXXXXXXXXXXXXXXXXXXX',
            'secret_access_key': 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
        },
        'profile_pic': {
            'template_name': 'profile_pic',
            'allowed_types': ['jpg', 'png'],
            'max_size': 500*1000, # 500 kb max
        },
        'videos': {
            'template_name': 'videos',
            'allowed_types': ['mp4', 'avi'],
            'max_size': 750*1000*1000, # 750 mb max
        },
    }


If 'default' is not specified, then 'S3UPLOADER' or the loose AWS confs will be
used.

Usage
==============

There are a few ways to use this module with your models.

Using GenericUploadableMixin
-------

### Single conf, file\_field

The fastest way to get started is by having your model extend the
GenericUploadableMixin. As such:


    from django.db import models
    from django.contrib.auth.models import User
    from s3uploader.models import GenericUploadableMixin

    ## Photo
    class Photo(models.Model, GenericUploadableMixin):

        class S3UploadableMeta:
            file_field = 'photo'
            is_ready_field = 'is_ready'

            @staticmethod
            def on_upload_start(photo, request):
                photo.ip_address = request.META['REMOTE_ADDR']
                if not request.user.is_authenticated():
                    raise ValueError("only valid users")
                photo.user = request.user

            @staticmethod
            def on_upload_end(photo, request):
                photo.ip_address = request.META['REMOTE_ADDR']

        user = models.ForeignKey(User)
        photo = models.FileField(upload_to="photos/")
        is_ready = models.BooleanField()

        # An example of some arbitrary fields
        ip_address = models.IPAddressField()


Then, configure something like this:

    S3UPLOADER = {
        'generic_model_class': 'myappname.Photo',
        'access_key_id': 'XXXXXXXXXXXXXXXXXXXX',
        'secret_access_key': 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
    }

Now, include an upload widget somewhere in your template, with the following:

    <head>
        {% s3uploader_css %}
    </head>

    <body>
        <h2>Drag &amp; drop photos below to upload</h2>
        {% s3uploader %}

        <script src="{{ STATIC_URL }}js/jquery.js"></script>
        {% s3uploader_js %}
    </body>


### Multiple conf, url\_field

For multiple profiles:


    # in settings.py ...
    S3UPLOADER_PROFILES = {
        'default': {
            'template_name': 'bootstrap',
            'access_key_id': 'XXXXXXXXXXXXXXXXXXXX',
            'secret_access_key': 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
        },
        'photo': {
            'generic_model_class': 'tests3uploader.Photo',
        },
        'url_photo': {
            'generic_model_class': 'tests3uploader.UrlPhoto',
        },
    }


    # in tests3uploader/models.py ...
    class UrlPhoto(models.Model, GenericUploadableMixin):
        class S3UploadableMeta:
            url_field = 'url'

            @staticmethod
            def generate_filename(photo, original_filename):
                return "photo2_%s" % (original_filename)
        url = models.URLField()

    # in your template ...
    {% s3uploader url_photo %}


Note that this new class just stores the URL instead of a file field, which
might be a better / more flexible approach when using cloud storage like this,
especially as you migrate from one cloud provider to another, or change bucket
configurations, etc.

Note also you probably want to specify where it ends up via the
`generate_filename` method.


### Updating file fields on S3Uploadable models, variables

Here's an example of how to add a button to replace the file in an S3Uploadable
model.


    # in models.py ...
    class ReplacePhoto(models.Model, GenericUploadableMixin):
        class S3UploadableMeta:
            file_field = 'photo'

            @staticmethod
            def get_object(request, original_filename, variables):
                if not variables.get('photo_id'):
                    p = ReplacePhoto() # new photo
                else: # replace an old photo
                    p = ReplacePhoto.objects.get(id=variables['photo_id'])
                p.category = variables.get('category')
                return p

        photo = models.FileField(upload_to="photos/")
        category = models.CharField(max_length=32, null=True, blank=True)

    # in your template ...
    {% s3uploader replacephoto photo_id=photo.id category="nature" %}

The "get\_object" method gets passed "variables" as the third element, which is
whatever you specify in your template, allowing you to better prepare the
object to be created based on the context of the request.

Note that variables that are embedded in the template are naturally open to be
tampered with, so you should double check in this function (or another) that
the given user has permissions to access that object.

Extending the backend
---------

If you want more control you might want to extend the backend directly.

For this look at `s3uploader/backend.py`.

Quick example:

    from s3uploader.backends import BackendBase

    class UploadLogger(BackendBase):
        # Override these...
        def starting_upload(self, original_filename, request, variables):
            # Do any sort of DB stuff etc to indicate that we are starting an upload.
            # Return the desired key to store in AWS
            # For example, store stuff in a cache:
            new_filename = "%s/%s" % (request.user.username, original_filename)
            cache.set('upload_started_' + request.user.username, new_filename)
            return new_filename

        def allow(self, request, variables):
            # For example, only allow authenticated users to upload:
            return request.is_authenticated()

        def success(self, request, variables):
            # User reports successful upload (note, this might not fire if the
            user is being sneaky, or if gets disconnected before).
            new_filename = cache.get('upload_started_' + request.user.username)
            UploadLog.objects.create(filename=new_filename)

            # Return a dict. Later this could be used to give feedback to JS UI
            return {}


    # in settings.py ...
    S3UPLOADER = {
        'backend': 'myappname.UploadLogger',
    }



Note that this example is not entirely functional (e.g. will have issues if the
same user tries uploading many things at once since the cache.set only goes by
the username), but it should give you an idea.

Gotchas
==============

* Right now, it requires `jquery` to be loaded before `{% s3uploader_js %}` --
  this requirement could be removed in the future, but for now it was the
  easiest thing to do.

* Make sure S3 is configured as per the tutorial 

More configuration options
==============


### Required:

    'bucket_name': 'openlab-testing',
    'access_key_id': 'XXXXXXXXXXXXXXXXXXXX',
    'secret_access_key': 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',

These are things you'll want to get from your amazon dashboard. You will
probably want to keep them in ENV variables instead of checked into your code,
along with  DB passwords etc.


    'backend': 'gallery.uploader_backends.PhotoBackend',

Import path to a class that extends the `s3uploader.backends.Backend` base
class, implementing necessary methods.

This is necessary since it controls the behavior of the uploaded, and lets you
record the uploads into the database.


### Appearances:

    'disable_css_classes': True,

Recommended if you are heavily customizing its look. Shortcut to disable a lot
of fineuploader's crufty CSS classes

    'template_name': 'bootstrap',


Specify which template to include for the fine uploader control. Default
options are 'default', 'simple-thumbnail', and 'bootstrap' (bootstrap 3)

To make your own template, simply put it in your templates search path (e.g. your templates directory) in a folder called "fineuploader".

For example: save your templates/fineuploader/custom.html, and then specify with:

    'template_name': 'custom',



### Behavior:




### Fine uploader:

        'options': {
            'classes': {
                'retryable': 'active',
                'retrying': 'active',
                'success': 'active',
            }
        }

Allows you to pass options directly to Fine Uplander. This gives you tons of control.

In this example we are adding some classes to the function on certain conditions


### JS options

Additional JS options can be attached to `window.S3_OPTIONS`. This allows you
to add in event hooks, etc.

